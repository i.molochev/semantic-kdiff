package ru.nsu.fit.molochev.semantic_diff.settings

import com.intellij.openapi.options.Configurable
import javax.swing.JComponent

class SemanticDiffSettingsConfigurable: Configurable {
    private var settingsComponent: SemanticDiffSettingsComponent? = null

    override fun createComponent(): JComponent? {
        settingsComponent = SemanticDiffSettingsComponent()
        return settingsComponent?.panel
    }

    override fun isModified(): Boolean {
        val settings = SemanticDiffSettingsState.instance
        return settingsComponent?.let { component ->
            component.ignoreWhitespaces != settings.ignoreWhitespaces
                    || component.leafNodesSimilarityThreshold / 10.0 != settings.leafNodesSimilarityThreshold
                    || component.innerNodesSimilarityThreshold / 10.0 != settings.innerNodesSimilarityThreshold
                    || component.similarityBoundaryCoefficient / 10.0 != settings.similarityBoundaryCoefficient
                    || component.strongSimilarityBoundaryPercentage / 10.0 != settings.strongSimilarityBoundaryPercentage
        } ?: false
    }

    override fun apply() {
        val settings = SemanticDiffSettingsState.instance
        settingsComponent?.apply {
            settings.ignoreWhitespaces = ignoreWhitespaces
            settings.innerNodesSimilarityThreshold = innerNodesSimilarityThreshold / 10.0
            settings.leafNodesSimilarityThreshold = innerNodesSimilarityThreshold / 10.0
            settings.similarityBoundaryCoefficient = similarityBoundaryCoefficient / 10.0
            settings.strongSimilarityBoundaryPercentage = strongSimilarityBoundaryPercentage / 10.0
        }


    }

    override fun reset() {
        val settings = SemanticDiffSettingsState.instance
        settingsComponent?.apply {
            ignoreWhitespaces = settings.ignoreWhitespaces
            innerNodesSimilarityThreshold = (settings.innerNodesSimilarityThreshold * 10).toInt()
            leafNodesSimilarityThreshold = (settings.innerNodesSimilarityThreshold * 10).toInt()
            similarityBoundaryCoefficient = (settings.similarityBoundaryCoefficient * 10).toInt()
            strongSimilarityBoundaryPercentage = (settings.strongSimilarityBoundaryPercentage * 10).toInt()
        }
    }

    override fun getDisplayName() = "Semantic Diff"

    override fun getPreferredFocusedComponent() = settingsComponent?.preferredFocusedComponent

    override fun disposeUIResources() {
        settingsComponent = null
    }
}
