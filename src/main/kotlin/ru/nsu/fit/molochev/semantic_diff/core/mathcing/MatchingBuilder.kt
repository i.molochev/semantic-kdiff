package ru.nsu.fit.molochev.semantic_diff.core.mathcing

import ru.nsu.fit.molochev.semantic_diff.core.DiffConfig
import ru.nsu.fit.molochev.semantic_diff.core.common.DiffTreeNode
import ru.nsu.fit.molochev.semantic_diff.core.mathcing.changedistilling.ChangeDistillerMatcher
import ru.nsu.fit.molochev.semantic_diff.core.mathcing.chawathe.FastMatcher
import ru.nsu.fit.molochev.semantic_diff.core.mathcing.chawathe.PostProcessor
import ru.nsu.fit.molochev.semantic_diff.core.mathcing.chawathe.PreProcessor
import ru.nsu.fit.molochev.semantic_diff.lang.LanguageSpecificExtensionsProvider

class MatchingBuilder(private val config: DiffConfig) {
    private val preProcessor = PreProcessor()
    private val chawatheMatcher = FastMatcher(config)
    private val postProcessor = PostProcessor(config)

    private val changeDistillerMatcher = ChangeDistillerMatcher(config)

    /*fun match(t1: DiffTreeNode, t2: DiffTreeNode, languageId: LanguageId?) =
      if (config.enableChangeDistilling)
        matchDistilling(t1, t2)
      else
        matchChawathe(t1, t2, languageId)*/

    fun match(t1: DiffTreeNode, t2: DiffTreeNode, provider: LanguageSpecificExtensionsProvider): Matching {
        val matching = Matching()

        val nodesBefore = t1.nodes()
        val nodesAfter = t2.nodes()

        preProcessor.match(nodesBefore, nodesAfter, matching, provider)

        if (config.enableChangeDistilling) {
            changeDistillerMatcher.match(nodesBefore, nodesAfter, matching)
        } else {
            chawatheMatcher.match(nodesBefore, nodesAfter, matching, provider)
        }

        provider.specificNodePostprocessor(nodesBefore, nodesAfter, matching)

        if (!matching.contains(t1) && !matching.contains(t2)) {
            matching.add(t1, t2)
        }

        postProcessor.treeHeight = t1.height()
        postProcessor.match(t1, matching)

        return matching
    }

//    fun matchChawathe(t1: DiffTreeNode, t2: DiffTreeNode, provider: LanguageSpecificExtensionsProvider): Matching {
//        val matching = Matching()
//
//        val nodesBefore = t1.nodes()
//        val nodesAfter = t2.nodes()
//
//        preProcessor.match(nodesBefore, nodesAfter, matching, languageId)
//        chawatheMatcher.match(nodesBefore, nodesAfter, matching, languageId)
//        provider.specificNodePostprocessor(nodesBefore, nodesAfter, matching)
//
//        if (!matching.contains(t1) && !matching.contains(t2)) {
//            matching.add(t1, t2)
//        }
//
//        postProcessor.treeHeight = t1.height()
//        postProcessor.match(t1, matching)
//
//        return matching
//    }

//    fun matchDistilling(t1: DiffTreeNode, t2: DiffTreeNode): Matching {
//        val matching = Matching()
//
//        val nodesBefore = t1.nodes()
//        val nodesAfter = t2.nodes()
//
//        changeDistillerMatcher.match(nodesBefore, nodesAfter, matching)
//
//        return matching
//    }
}

class Matching {
    val pairs = mutableListOf<Pair<DiffTreeNode, DiffTreeNode>>()

    fun partner(x: DiffTreeNode?): DiffTreeNode? {
        if (x == null) return null
        for (pair in pairs) {
            if (pair.first === x) return pair.second
            if (pair.second === x) return pair.first
        }
        return null
    }

    fun contains(x: DiffTreeNode?): Boolean {
        if (x == null) return false
        return pairs.any { it.first === x || it.second === x }
    }

    fun contains(x1: DiffTreeNode?, x2: DiffTreeNode?): Boolean {
        if (x1 == null || x2 == null) return false
        return pairs.any { it.first === x1 && it.second === x2 }
    }

    fun add(pair: Pair<DiffTreeNode, DiffTreeNode>) {
        pairs.add(pair)
    }

    fun add(x1: DiffTreeNode, x2: DiffTreeNode) {
        pairs.add(Pair(x1, x2))
    }

    fun removeWith(node: DiffTreeNode) = pairs.removeIf { it.first === node || it.second === node }
}
