package ru.nsu.fit.molochev.semantic_diff.core.mathcing.chawathe

import ru.nsu.fit.molochev.semantic_diff.core.common.DiffTreeNode
import ru.nsu.fit.molochev.semantic_diff.core.common.isOfType
import ru.nsu.fit.molochev.semantic_diff.core.mathcing.Matching
import ru.nsu.fit.molochev.semantic_diff.lang.LanguageSpecificExtensionsProvider

class PreProcessor {

    fun match(nodesBefore: List<DiffTreeNode>, nodesAfter: List<DiffTreeNode>, matching: Matching, provider: LanguageSpecificExtensionsProvider) {
        matchUnique(nodesBefore, nodesAfter, matching, provider)

        val canNodesBeEqualWithContext = provider.contextNodeComparator

        val labels = nodesBefore.filter { it.id != null }.map(DiffTreeNode::label).distinct()
        for (label in labels) {
            val labeledBefore = nodesBefore.filter { it.label == label }
            val labeledAfter = nodesAfter.filter { it.label == label }

            val ids = labeledBefore.mapNotNull(DiffTreeNode::id).distinct()
            for (id in ids) {
                val beforeWithId = labeledBefore.filter { it.id == id }
                val afterWithId = labeledAfter.filter { it.id == id }

                if (beforeWithId.size == 1 && afterWithId.size == 1) {
                    val identifiedBefore = beforeWithId.first()
                    val identifiedAfter = afterWithId.first()
                    matchIdentified(identifiedBefore, identifiedAfter, matching)
                } else {
                    for (beforeCandidate in beforeWithId) {
                        val correctWithId = afterWithId.filter { canNodesBeEqualWithContext(beforeCandidate, it) }
                        if (correctWithId.size == 1) {
                            matchIdentified(beforeCandidate, correctWithId.first(), matching)
                        }
                    }
                }
            }
        }
    }

    private fun matchIdentified(identifiedBefore: DiffTreeNode, identifiedAfter: DiffTreeNode, matching: Matching) {
        matching.add(identifiedBefore, identifiedAfter)

        for (beforeChild in identifiedBefore.children) {    // TODO: does always works correctly?
            val partner = identifiedAfter.children.find { afterChild ->
                !matching.contains(beforeChild) && !matching.contains(afterChild)
                        && beforeChild.label == afterChild.label
                        && (!beforeChild.isLeaf() && !afterChild.isLeaf() // TODO: Can it match different inner children?
                        || beforeChild.text == afterChild.text)
            }

            if (partner != null) {
                matching.add(beforeChild, partner)
            }
        }
    }

    private fun matchUnique(nodesBefore: List<DiffTreeNode>, nodesAfter: List<DiffTreeNode>, matching: Matching, provider: LanguageSpecificExtensionsProvider) {
        for (node in provider.uniqueNodes) {
            val left = nodesBefore.find { it.value.isOfType(node) }
            val right = nodesAfter.find { it.value.isOfType(node) }
            if (left != null && right != null) {
                matching.add(left, right)
            }
        }
    }
}
