package ru.nsu.fit.molochev.semantic_diff.core.mathcing.chawathe

import ru.nsu.fit.molochev.semantic_diff.core.DiffConfig
import ru.nsu.fit.molochev.semantic_diff.core.common.DiffTreeNode
import ru.nsu.fit.molochev.semantic_diff.core.common.LongestCommonSubsequence
import ru.nsu.fit.molochev.semantic_diff.core.common.commonScore
import ru.nsu.fit.molochev.semantic_diff.core.common.levenshteinSimilarity
import ru.nsu.fit.molochev.semantic_diff.core.mathcing.Matching
import ru.nsu.fit.molochev.semantic_diff.lang.LanguageSpecificExtensionsProvider

class FastMatcher(private val config: DiffConfig) {

    fun match(
        nodesBefore: List<DiffTreeNode>,
        nodesAfter: List<DiffTreeNode>,
        matching: Matching,
        provider: LanguageSpecificExtensionsProvider
    ) {
        val leafLabelsBefore = nodesBefore.filter { it.isLeaf() && !matching.contains(it) }.map { it.label }

        val nodesToMatchBefore = nodesBefore.filter { it.isLeaf() && !matching.contains(it) }.toMutableList()
        val nodesToMatchAfter = nodesAfter.filter { it.isLeaf() && !matching.contains(it) }.toMutableList()

        while (nodesToMatchBefore.isNotEmpty() || nodesToMatchAfter.isNotEmpty()) {
            val lcs = LongestCommonSubsequence
                .find(nodesToMatchBefore, nodesToMatchAfter) { x, y -> areNodesEqual(x, y, matching, provider) }
            lcs.forEach(matching::add)

            val unmatchedFromBefore = nodesToMatchBefore.filter { !matching.contains(it) }
            val unmatchedFromAfter = nodesToMatchAfter.filter { !matching.contains(it) }
            val newlyMatched = mutableListOf<Pair<DiffTreeNode, DiffTreeNode>>()
            for (node in unmatchedFromBefore) {
                val forMatch = unmatchedFromAfter
                    .filter { areNodesEqual(node, it, matching, provider) }
                if (forMatch.size == 1) {   // TODO: Should we just take first? Or introduce 'compare'?
                    val match = forMatch.first()
                    matching.add(node, match)
                    newlyMatched.add(Pair(node, match))
                }
            }

            nodesToMatchBefore.clear()
            nodesToMatchAfter.clear()

            for (pair in (lcs + newlyMatched)) {
                val parentBefore = pair.first.parent
                if (parentBefore != null
                    && !nodesToMatchBefore.contains(parentBefore)
                    && !matching.contains(parentBefore)
                    && parentBefore.childrenMatched(matching)) {
                    nodesToMatchBefore.add(parentBefore)
                }

                val parentAfter = pair.second.parent
                if (parentAfter != null
                    && !nodesToMatchAfter.contains(parentAfter)
                    && !matching.contains(parentAfter)
                    && parentAfter.childrenMatched(matching)) {
                    nodesToMatchAfter.add(parentAfter)
                }
            }
        }
    }

    //fun fastMatch(nodesBefore: List<DiffTreeNode>, nodesAfter: List<DiffTreeNode>, matching: Matching) {
    //  val labels = (nodesBefore + nodesAfter).map { it.label }.toSet()
    //
    //  val leafLabelsBefore = nodesBefore.filter { it.isLeaf() }.map { it.label }.toSet()
    //  val leafLabelsAfter = nodesAfter.filter { it.isLeaf() }.map { it.label }.toSet()
    //  val leafLabels = leafLabelsBefore union leafLabelsAfter
    //
    //  for (label in leafLabels) {
    //    matchLabel(nodesBefore.filter { it.label == label }, nodesAfter.filter { it.label == label })
    //  }
    //}
    //
    //private fun matchLabel(nodesBefore: List<DiffTreeNode>, nodesAfter: List<DiffTreeNode>, matching: Matching) {
    //  val lcs = LongestCommonSubsequence.find(nodesBefore, nodesAfter) { x, y -> areNodesEqual(x, y, matching) }
    //  lcs.forEach(matching::add)
    //
    //  val unmatchedFromBefore = nodesBefore.filter { !matching.contains(it) }
    //  val unmatchedFromAfter = nodesAfter.filter { !matching.contains(it) }
    //  for (node in unmatchedFromBefore) {
    //    val forMatch = unmatchedFromAfter
    //      .filter { areNodesEqual(node, it, matching) }
    //    if (forMatch.size == 1) {
    //      val match = forMatch.first()
    //      matching.add(node, match)
    //    }
    //  }
    //}

    private fun DiffTreeNode.childrenMatched(matching: Matching) = children.all(matching::contains)

    private fun areNodesEqual(
        x: DiffTreeNode,
        y: DiffTreeNode,
        matching: Matching,
        provider: LanguageSpecificExtensionsProvider
    ): Boolean {
        if (x.label != y.label) return false

        val canNodesBeEqualWithContext = provider.contextNodeComparator

        if (!canNodesBeEqualWithContext(x, y)) return false

        val areEqual = if (x.isLeaf() && y.isLeaf()) {
            x.label == y.label && (x.text == y.text || levenshteinSimilarity(x.text, y.text) >= config.leafNodesSimilarityThreshold)
        } else {
            val commonScore = commonScore(x, y, matching)
            commonScore >= config.innerNodesSimilarityThreshold
        }

        return areEqual || provider.specificNodeEqualizer(x, y)
    }
}
