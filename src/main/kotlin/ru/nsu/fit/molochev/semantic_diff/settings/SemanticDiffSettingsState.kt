package ru.nsu.fit.molochev.semantic_diff.settings

import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage
import com.intellij.util.xmlb.XmlSerializerUtil
import ru.nsu.fit.molochev.semantic_diff.core.DiffConfig

@State(
    name = "ru.nsu.fit.molochev.semantic_diff.settings.SemanticDiffSettingsState",
    storages = [Storage("SemanticDiffPlugin.xml")]
)
class SemanticDiffSettingsState(
    var ignoreWhitespaces: Boolean = true,
    var leafNodesSimilarityThreshold: Double = 0.6,
    var innerNodesSimilarityThreshold: Double = 0.2,
    var similarityBoundaryCoefficient: Double = 2.0,
    var strongSimilarityBoundaryPercentage: Double = 0.9
) : PersistentStateComponent<SemanticDiffSettingsState> {

    companion object {
        val instance
            get() = ApplicationManager.getApplication().getService(SemanticDiffSettingsState::class.java)
    }

    override fun getState() = this

    override fun loadState(state: SemanticDiffSettingsState) {
        XmlSerializerUtil.copyBean(state, this)
    }

    fun toConfig() = DiffConfig(
        leafNodesSimilarityThreshold,
        innerNodesSimilarityThreshold,
        similarityBoundaryCoefficient,
        strongSimilarityBoundaryPercentage = strongSimilarityBoundaryPercentage
    )
}
