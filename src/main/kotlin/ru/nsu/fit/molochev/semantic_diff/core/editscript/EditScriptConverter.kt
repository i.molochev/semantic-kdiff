package ru.nsu.fit.molochev.semantic_diff.core.editscript

import com.intellij.openapi.util.TextRange
import ru.nsu.fit.molochev.semantic_diff.lang.LanguageSpecificExtensionsProvider

import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

data class SemanticSideBySideDiff(val fragments: SemanticDiffFragments)

typealias SemanticDiffFragments = List<SemanticDiffFragment>

class SemanticDiffFragment(op: EditOperation) {

    var type: EditOperationType = op.type
        private set

    var leftRange: TextRange = op.leftRange()
        private set

    var rightRange: TextRange = op.rightRange()
        private set

    private val operations = mutableListOf(op)

    fun merge(op: EditOperation): Boolean {
        if (op.type != type) return false

        if (operations.isEmpty()) {
            add(op)
            return true
        }

        return when (type) {
            EditOperationType.MOVE, EditOperationType.UPDATE -> false
            //EditOperationType.UPDATE -> {
            //  mergeNeighbour(op)
            //}
            //EditOperationType.MOVE -> {
            //  if (op.leftRange() != op.rightRange()) {
            //    mergeNeighbour(op)
            //  } else false
            //}
            EditOperationType.INSERT -> {
                if (mergeParent(op)) {
                    return true
                }

                val left = (operations.map { it.leftStart } + op.leftStart).minOrNull() ?: op.leftStart

                if (operations.any {
                        it.rightRange().intersects(op.rightRange()) || it.rightRange().neighbours(op.rightRange())
                    }) return add(op, overriddenLeft = TextRange(left, left))

                (op as Insert).let { insert ->
                    if (operations.any {
                            val myParent = it.dst ?: return@any false
                            myParent == insert.dst && abs(it.index - insert.index) <= 1
                        }) add(insert, overriddenLeft = TextRange(left, left))
                    else false
                }
            }
            EditOperationType.DELETE -> {
                if (mergeParent(op)) {
                    return true
                }

                val right = (operations.map { it.rightStart } + op.rightStart).minOrNull() ?: op.rightStart

                if (operations.any {
                        it.leftRange().intersects(op.leftRange()) || it.leftRange().neighbours(op.leftRange())
                    }) return add(op, overriddenRight = TextRange(right, right))

                (op as Delete).let { delete ->
                    if (operations.any {
                            val myParent = (it as Delete).oldParent ?: return@any false
                            myParent == delete.oldParent && abs(it.index - delete.index) <= 1
                        }) add(delete, overriddenRight = TextRange(right, right))
                    else false
                }
            }
        }
    }

    private fun mergeParent(op: EditOperation): Boolean {
        val parent = operations.find { op.src.haveParent(it.src) } ?:
        op.takeIf { operations.any{ it.src.haveParent(op.src) } }
        return parent?.let {
            if (type == EditOperationType.INSERT) {
                add(op, overriddenLeft = it.leftRange())
            } else {
                add(op, overriddenRight = it.rightRange())
            }
        } ?: false
    }

    private fun mergeNeighbour(op: EditOperation): Boolean {
        val otherLeft = op.leftRange()
        val otherRight = op.rightRange()
        val intersects = leftRange.intersects(otherLeft) == true && rightRange.intersects(otherRight) == true
        val neighbours = leftRange.neighbours(otherLeft) == true && rightRange.neighbours(otherRight) == true

        return if (intersects || neighbours) {
            add(op)
        } else false
    }

    private fun add(op: EditOperation, overriddenLeft: TextRange? = null, overriddenRight: TextRange? = null): Boolean {
        leftRange = overriddenLeft ?: leftRange.union(op.leftRange())
        rightRange = overriddenRight ?: rightRange.union(op.rightRange())
        operations.add(op)
        return true
    }

    override fun toString(): String {
        return "$type ${leftRange.startOffset}:${leftRange.endOffset} -> ${rightRange.startOffset}:${rightRange.endOffset}"
    }
}

fun TextRange.neighbours(other: TextRange?) =
    if (other == null) false
    else max(startOffset, other.startOffset) == min(endOffset, other.endOffset)

private data class MarkedOperation(val operation: EditOperation, var processed: Boolean = false)

fun convert(editScript: EditScript, provider: LanguageSpecificExtensionsProvider): SemanticSideBySideDiff {
    // Language-specific heuristics
    val optimize = provider.editScriptOptimizer

    val markedOperations = optimize(editScript).operations.map { MarkedOperation(it) }.sortedBy { it.operation.leftStart }
    val fragments = mutableListOf<SemanticDiffFragment>()

    val inserts = markedOperations.filter { it.operation.type == EditOperationType.INSERT }.sortedBy { it.operation.rightStart }

    // Separately group inserts
    groupOperations(inserts, fragments)
    // And the all other operations
    groupOperations(markedOperations, fragments)

    return SemanticSideBySideDiff(fragments.sortedBy { it.leftRange.startOffset })
}

private fun groupOperations(operations: List<MarkedOperation>, fragments: MutableList<SemanticDiffFragment>) {
    operations.forEach { op ->
        if (op.processed) return@forEach

        val fragment = SemanticDiffFragment(op.operation)
        op.processed = true

        operations
            .filter { !it.processed }
            .forEach { it.processed = fragment.merge(it.operation) }

        if (fragment.type != EditOperationType.MOVE || fragment.leftRange != fragment.rightRange) {
            fragments.add(fragment)
        }
    }
}
