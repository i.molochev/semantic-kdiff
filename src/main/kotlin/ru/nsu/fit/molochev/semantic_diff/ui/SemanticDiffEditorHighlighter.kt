package ru.nsu.fit.molochev.semantic_diff.ui

import com.intellij.openapi.editor.Editor
import com.intellij.openapi.editor.markup.HighlighterLayer
import com.intellij.openapi.editor.markup.HighlighterTargetArea
import com.intellij.openapi.editor.markup.TextAttributes
import ru.nsu.fit.molochev.semantic_diff.core.editscript.EditOperationType
import ru.nsu.fit.molochev.semantic_diff.core.editscript.SemanticSideBySideDiff
import java.awt.Color

private val textColor = Color(50, 50, 50)
private val moved = TextAttributes(textColor, Color(221, 163, 239), null, null, 0)
private val updated = TextAttributes(textColor, Color(175, 220, 255), null, null, 0)
private val inserted = TextAttributes(textColor, Color(161, 255, 173), null, null, 0)
private val deleted = TextAttributes(textColor, Color(255, 120, 125), null, null, 0)

private val layer = HighlighterLayer.CARET_ROW
private val area = HighlighterTargetArea.EXACT_RANGE

private val colors = mapOf(
    EditOperationType.INSERT to inserted,
    EditOperationType.MOVE to moved,
    EditOperationType.DELETE to deleted,
    EditOperationType.UPDATE to updated
)

fun addHighlighting(diff: SemanticSideBySideDiff, leftEditor: Editor, rightEditor: Editor) {
    val leftMarkup = leftEditor.markupModel
    val rightMarkup = rightEditor.markupModel

    for (fragment in diff.fragments) {

        val type = fragment.type
        val color = colors[type]

        val leftRange = fragment.leftRange
        if (type != EditOperationType.DELETE) {
            leftMarkup.addRangeHighlighter(leftRange.startOffset, leftRange.endOffset, layer, color, area)
        }

        val righttRange = fragment.rightRange
        if (type != EditOperationType.INSERT) {
            rightMarkup.addRangeHighlighter(righttRange.startOffset, righttRange.endOffset, layer, color, area)
        }
    }
}
