package ru.nsu.fit.molochev.semantic_diff.core.editscript

import com.intellij.refactoring.suggested.endOffset
import com.intellij.refactoring.suggested.startOffset
import ru.nsu.fit.molochev.semantic_diff.core.common.DiffTreeNode
import ru.nsu.fit.molochev.semantic_diff.core.common.LongestCommonSubsequence
import ru.nsu.fit.molochev.semantic_diff.core.mathcing.Matching

class EditScript {
    val operations = mutableListOf<EditOperation>()


    fun addAndExecOp(op: EditOperation) {
        operations.add(op)
        op.exec()
    }

    override fun toString(): String {
        return operations.joinToString("\n", transform = EditOperation::toString)
    }
}

fun buildScript(
    beforeTree: DiffTreeNode,
    afterTree: DiffTreeNode,
    matching: Matching
): EditScript {
    val script = EditScript()
    val nodes = ArrayDeque<DiffTreeNode>()
    nodes.add(afterTree)
    bfs(nodes, script, matching)
    deleteUnmatched(beforeTree, script, matching)
    return script
}

private fun bfs(nodes: ArrayDeque<DiffTreeNode>, script: EditScript, matching: Matching) {
    while (!nodes.isEmpty()) {
        val node = nodes.removeFirst()                  // x
        val parent = node.parent                        // y
        var partner = matching.partner(node)            // w
        val parentPartner = matching.partner(parent)    // z
        if (partner == null && parentPartner != null) {
            // Insert
            partner = node.copy()
            //println(partner)
            val pos = findPosition(node, matching) { matching.contains(it) }
            script.addAndExecOp(Insert(partner, parentPartner, pos))
            matching.add(partner, node)
        }
        else if (partner != null && parent != null) {
            //println(partner)
            // Update
            if (node.isLeaf() && node.text != partner.text) {
                script.addAndExecOp(Update(partner.copy(), node.value))
            }
            //Move
            if (!matching.contains(partner.parent, parent) && parentPartner != null) {
                val k = findPosition(node, matching) { matching.contains(it) }
                script.addAndExecOp(Move(partner, parentPartner, k, node.value.startOffset, node.value.endOffset))
            }
        }
        //println(partner)
        // Align
        if (partner != null) {
            alignChildren(partner, node, script, matching)
        }

        node.children.forEach(nodes::addLast)
    }
}

private fun findPosition(node: DiffTreeNode, matching: Matching, inOrder: (DiffTreeNode) -> Boolean): Int {
    val parent = node.parent ?: return 0              // y

    val index = parent.children.indexOf(node)
    if (index == 0) return 0

    var leftSiblingIndex = index - 1
    var leftSibling = parent.children[leftSiblingIndex]       // v
    while (leftSiblingIndex >= 0 && !inOrder(leftSibling)) {
        leftSibling = parent.children[leftSiblingIndex--]
    }
    if (!inOrder(leftSibling)) return 0


    val leftSiblingPartner = matching.partner(leftSibling)    // u
    val leftSiblingPartnerIndex = leftSiblingPartner?.parent?.children
        ?.filter(inOrder)
        ?.indexOf(leftSiblingPartner)
        ?: -1

    return leftSiblingPartnerIndex + 1
}

private fun alignChildren(node1: DiffTreeNode, node2: DiffTreeNode, script: EditScript, matching: Matching) {
    val s1 = node2.children.mapNotNull(matching::partner).filter(node1.children::contains)
    val s2 = node1.children.mapNotNull(matching::partner).filter(node2.children::contains)

    val s = LongestCommonSubsequence.find(s1, s2, matching::contains)

    val unordered = matching.pairs
        .filter { s1.contains(it.first) && s2.contains(it.second) }
        .filter { !s.contains(it) }
    val ordered = s.toMutableList()

    for (pair in unordered) {
        val pos = findPosition(pair.second, matching) { el -> ordered.any { it.second === el } }
        script.addAndExecOp(Move(pair.first, node1, pos, pair.second.value.startOffset, pair.second.value.endOffset))
        ordered.add(pair)
    }
}

private fun deleteUnmatched(node: DiffTreeNode, script: EditScript, matching: Matching) {
    val deletes = mutableListOf<Delete>()
    for (child in node.children) {
        if (!child.isLeaf()) {
            deleteUnmatched(child, script, matching)
        }
        else if (!matching.contains(child)) {
            deletes.add(Delete(child))
        }
    }
    deletes.forEach(script::addAndExecOp)
}
