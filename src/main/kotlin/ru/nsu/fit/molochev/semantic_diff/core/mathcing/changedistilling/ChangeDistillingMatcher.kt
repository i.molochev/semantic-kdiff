package ru.nsu.fit.molochev.semantic_diff.core.mathcing.changedistilling

import ru.nsu.fit.molochev.semantic_diff.core.DiffConfig
import ru.nsu.fit.molochev.semantic_diff.core.common.DiffTreeNode
import ru.nsu.fit.molochev.semantic_diff.core.common.commonScore
import ru.nsu.fit.molochev.semantic_diff.core.common.ngramsSimilarity
import ru.nsu.fit.molochev.semantic_diff.core.mathcing.Matching

class ChangeDistillerMatcher(private val config: DiffConfig) {

    companion object {
        private const val WEIGHTING_THRESHOLD = 0.8
    }

    fun match(nodesBefore: List<DiffTreeNode>, nodesAfter: List<DiffTreeNode>, matching: Matching) {
        matchLeaves(nodesBefore, nodesAfter, matching).sortedByDescending { it.similarity }.forEach {
            if (!matching.contains(it.x) && !matching.contains(it.y)) matching.add(it.x, it.y)
        }
        matchInnerNodes(nodesBefore, nodesAfter, matching)
    }

    private fun matchLeaves(nodesBefore: List<DiffTreeNode>, nodesAfter: List<DiffTreeNode>, matching: Matching): List<Matched> {
        val matchedLeaves = mutableListOf<Matched>()
        for (x in nodesBefore) {
            if (!x.isLeaf() || matching.contains(x)) continue
            for (y in nodesAfter) {
                if (!y.isLeaf() || y.label != x.label || matching.contains(y)) continue
                val similarity = ngramsSimilarity(x.text, y.text, config.ngramsCount)
                if (similarity >= config.leafNodesSimilarityThreshold) {
                    matchedLeaves.add(Matched(x, y, similarity))
                }
            }
        }
        return matchedLeaves
    }

    private fun matchInnerNodes(nodesBefore: List<DiffTreeNode>, nodesAfter: List<DiffTreeNode>, matching: Matching) {
        for (x in nodesBefore) {
            if (matching.contains(x) || (x.isLeaf() && x.parent != null)) continue
            for (y in nodesAfter) {
                if (matching.contains(x)) break
                if (y.isLeaf() || matching.contains(y) || !equal(x, y, matching)) continue
                matching.add(x, y)
            }
        }
    }

    private fun equal(x: DiffTreeNode, y: DiffTreeNode, matching: Matching): Boolean {
        if (x.label != y.label) return false

        var threshold = config.innerNodesSimilarityThreshold

        val dynamicThreshold = config.dynamicSimilarityThreshold
        val depthBound = config.dynamicSimilarityDepthBound
        if (config.isDynamicSimilarityEnabled
            && depthBound != null && dynamicThreshold != null
            && x.sentence().size < depthBound
            && y.sentence().size < depthBound
        ) {
            threshold = dynamicThreshold
        }

        val descendantsSimilarity = commonScore(x, y, matching)
        val valueSimilarity = ngramsSimilarity(x.text, y.text, config.ngramsCount)  // TODO: do we need to check 'plain' similarity?
        // TODO: should we just use node's text or maintain specific node's 'value'?

        return if (valueSimilarity < config.leafNodesSimilarityThreshold && descendantsSimilarity >= WEIGHTING_THRESHOLD) true
        else descendantsSimilarity >= threshold && valueSimilarity >= config.leafNodesSimilarityThreshold
    }

    private data class Matched(val x: DiffTreeNode, val y: DiffTreeNode, val similarity: Double)
}
