package ru.nsu.fit.molochev.semantic_diff.core.mathcing.chawathe

import ru.nsu.fit.molochev.semantic_diff.core.DiffConfig
import ru.nsu.fit.molochev.semantic_diff.core.common.DiffTreeNode
import ru.nsu.fit.molochev.semantic_diff.core.common.LongestCommonSubsequence
import ru.nsu.fit.molochev.semantic_diff.core.mathcing.Matching

import kotlin.math.max

class PostProcessor(
    private val config: DiffConfig,
    var treeHeight: Int = 0
) {

    fun match(node: DiffTreeNode, matching: Matching) {
        val partner = matching.partner(node) ?: return
        for (child in node.children) {
            val childPartner = matching.partner(child)
            var match: DiffTreeNode? = null

            if (childPartner == null) {
                val partnerMatchedChildren = partner.children.filter(matching::contains)
                val partnerUnmatchedChildren = partner.children.filter { !matching.contains(it) }

                val bestMatched = findBestPartner(child, partnerMatchedChildren)
                val bestUnmatched = findBestPartner(child, partnerUnmatchedChildren)

                when {
                    bestUnmatched == null && bestMatched == null -> {
                        child.removeFromMatchingRecursively(matching)
                        continue
                    }
                    bestUnmatched != null && bestMatched == null -> {
                        match = bestUnmatched
                    }
                    bestUnmatched == null && bestMatched != null -> {
                        match = bestMatched
                    }
                    else -> {
                        val realPartnerOfMatched = matching.partner(bestMatched)
                        val isOurChild = realPartnerOfMatched?.parent == node
                        match = if (isOurChild) bestMatched else bestUnmatched
                    }
                }
            } else if (childPartner.parent !== partner) {
                match = findBestPartner(child, partner.children)
                if (match == null/* && !config.areNodesCompatible(child, childPartner)*/) {   // TODO: nodes compatibility EP?
                    child.removeFromMatchingRecursively(matching)
                    continue
                }
            }

            if (match != null) {
                matching.removeWith(child)
                matching.removeWith(match)
                matching.add(child, match)
            }

            match(child, matching)
        }
    }

    private fun findBestPartner(node: DiffTreeNode, candidates: List<DiffTreeNode>): DiffTreeNode? {

        //val estimator = if (node.isLeaf()) { x, y, _ -> FastMatcher.similarity(x.text, y.text) } else FastMatcher::commonScore
        //
        //return candidates.filter { it.label == node.label }
        //  .map{ Pair(estimator(node, it, matching), it) }
        //  .sortedByDescending { it.first }
        //  .map { it.second }
        //  .firstOrNull()

        val boundPercentage = config.similarityBoundaryCoefficient * (1 - node.height() * 1.0 / treeHeight)

        var maxPercentage = 0.0
        var partner: DiffTreeNode? = null

        candidates.filter { it.label == node.label }
            .forEach {
                val maxLength = max(node.text.length, it.text.length)
                val lcs = LongestCommonSubsequence.find(
                    node.text.asIterable().toList(),
                    it.text.asIterable().toList(),
                    Char::equals)
                val percentage = lcs.size * 1.0 / maxLength
                if (percentage > maxPercentage) {
                    maxPercentage = percentage
                    partner = it
                }
            }

        if (node.isLeaf()) return if (maxPercentage >= 1.0) partner else null

        if (node.id != null) {
            return if (node.id == partner?.id || maxPercentage > config.strongSimilarityBoundaryPercentage) {
                partner
            }
            else null
        }

        return if (maxPercentage > boundPercentage) partner else null
    }

    private fun DiffTreeNode.removeFromMatchingRecursively(matching: Matching) {
        matching.removeWith(this)
        children.forEach { it.removeFromMatchingRecursively(matching) }
    }
}
