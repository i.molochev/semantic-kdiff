package ru.nsu.fit.molochev.semantic_diff.lang

import com.intellij.lang.Language
import com.intellij.psi.PsiElement
import com.intellij.psi.tree.IElementType
import org.jetbrains.kotlin.lexer.KtTokens
import org.jetbrains.kotlin.psi.*
import org.jetbrains.kotlin.psi.psiUtil.allChildren
import ru.nsu.fit.molochev.semantic_diff.core.common.DiffContextLevel
import ru.nsu.fit.molochev.semantic_diff.core.common.DiffTreeNode
import ru.nsu.fit.molochev.semantic_diff.core.common.isOfType
import ru.nsu.fit.molochev.semantic_diff.core.editscript.EditScript
import ru.nsu.fit.molochev.semantic_diff.core.mathcing.Matching
import kotlin.reflect.KClass

sealed class KotlinDiffContextLevel(contextHolder: DiffTreeNode, holderValue: String?) :
    DiffContextLevel(contextHolder, holderValue) {
    abstract fun copy(): KotlinDiffContextLevel

    data class TopLevelCtx(val contextHolder: DiffTreeNode, val holderValue: String? = null) :
        KotlinDiffContextLevel(contextHolder, holderValue) {
        override fun copy() = copy(contextHolder = contextHolder, holderValue = holderValue)
    }

    data class FunctionCtx(val contextHolder: DiffTreeNode, val holderValue: String? = null) :
        KotlinDiffContextLevel(contextHolder, holderValue) {
        override fun copy() = copy(contextHolder = contextHolder, holderValue = holderValue)
    }

    data class ClassCtx(val contextHolder: DiffTreeNode, val holderValue: String? = null) :
        KotlinDiffContextLevel(contextHolder, holderValue) {
        override fun copy() = copy(contextHolder = contextHolder, holderValue = holderValue)
    }

    data class PropertyCtx(val contextHolder: DiffTreeNode, val holderValue: String? = null) :
        KotlinDiffContextLevel(contextHolder, holderValue) {
        override fun copy() = copy(contextHolder = contextHolder, holderValue = holderValue)
    }
}

class KotlinDiffProvider : LanguageSpecificExtensionsProvider(
    Language.findLanguageByID("kotlin") ?: Language.ANY
) {
    override val contextLevelInitializer: DiffTreeNode.() -> Unit = {
        contextLevel = run {
            val parent = parent ?: return@run KotlinDiffContextLevel.TopLevelCtx(this)
            val parentLvl = parent.contextLevel as? KotlinDiffContextLevel ?: return@run null

            return@run when (parent.value::class) {
                is KtFunction -> KotlinDiffContextLevel.FunctionCtx(parent)
                is KtClassOrObject -> KotlinDiffContextLevel.ClassCtx(parent)
                is KtProperty -> KotlinDiffContextLevel.PropertyCtx(parent)
                else -> parentLvl.copy()
            }
        }
    }

    override val contextNodeComparator = fun(x: DiffTreeNode, y: DiffTreeNode): Boolean {
        val xContextLevel = x.contextLevel
        val yContextLevel = y.contextLevel
        if (xContextLevel == null && yContextLevel == null) return true
        if (xContextLevel != null && yContextLevel != null) {
            if (xContextLevel::class != yContextLevel::class) return false

            val xHolderId = xContextLevel.ctxHolder.id
            val yHolderId = yContextLevel.ctxHolder.id
            if (xHolderId == null && yHolderId == null) {
                return xContextLevel.value == yContextLevel.value
            }
            return xHolderId == yHolderId
        }
        return false
    }

    override val specificNodeEqualizer: (DiffTreeNode, DiffTreeNode) -> Boolean = { x, y ->
        if (x.label is KtFunction) {
            fun DiffTreeNode.getParameterTypes(): List<String> {
                return children.find { it.value is KtValueArgumentList }
                    ?.children?.filter { it.value is KtValueArgument }
                    ?.mapNotNull { it.children.find { c -> c.value is KtTypeReference } }
                    ?.map { it.text } ?: listOf()
            }

            val xName = x.children.find { it.label == KtTokens.IDENTIFIER }
            val yName = y.children.find { it.label == KtTokens.IDENTIFIER }
            val xParamTypes = x.getParameterTypes()
            val yParamTypes = y.getParameterTypes()

            xName != null && yName != null && xName == yName && xParamTypes == yParamTypes
        } else false
    }

    override val childrenIterator: (PsiElement) -> Iterator<PsiElement> = { node ->
        node.allChildren.iterator()
    }

    override val isWord: (IElementType) -> Boolean = { type -> type == KtTokens.IDENTIFIER }

    override val nodesToIgnore: List<KClass<out PsiElement>> = listOf()

    override val uniqueNodes: List<KClass<out PsiElement>> = listOf(KtPackageDirective::class, KtImportList::class)

    override val additionalNodeIdentifier: (DiffTreeNode) -> Unit = { node ->
        node.id = when {
            node.value.isOfType(KtDotQualifiedExpression::class) -> node.value.text.toString()
            node.value.isOfType(KtImportDirective::class) -> node.children
                .find { it.value.isOfType(KtDotQualifiedExpression::class) }?.id
            node.value is KtFunction -> null
            else -> node.id
        }
    }

    override val specificNodePostprocessor: (List<DiffTreeNode>, List<DiffTreeNode>, Matching) -> Unit = { _, _, _ -> }

    override val editScriptOptimizer: (EditScript) -> EditScript = { e -> e }

}
