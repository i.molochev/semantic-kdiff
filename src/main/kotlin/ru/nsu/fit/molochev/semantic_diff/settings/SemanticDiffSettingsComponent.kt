package ru.nsu.fit.molochev.semantic_diff.settings

import com.intellij.ui.components.JBCheckBox
import com.intellij.ui.components.JBLabel
import com.intellij.ui.components.JBSlider
import com.intellij.util.ui.FormBuilder
import java.util.Hashtable
import javax.swing.JLabel
import kotlin.math.pow
import javax.swing.JPanel

class SemanticDiffSettingsComponent {
    private val _ignoreWhitespaces = JBCheckBox("Ignore whitespaces")
    private val _leafNodesSimilarityThreshold = TenthSlider(0, 10)
    private val _innerNodesSimilarityThreshold = TenthSlider(0, 10)
    private val _similarityBoundaryCoefficient = TenthSlider(10, 100)
    private val _strongSimilarityBoundaryPercentage = TenthSlider(0, 10)

    val panel = FormBuilder.createFormBuilder()
        .addComponent(_ignoreWhitespaces)
        .addLabeledComponent(JBLabel("Leaf node similarity threshold: "), _leafNodesSimilarityThreshold)
        .addLabeledComponent(JBLabel("Inner node similarity threshold: "), _innerNodesSimilarityThreshold)
        .addLabeledComponent(JBLabel("Similarity boundary coefficient: "), _similarityBoundaryCoefficient)
        .addLabeledComponent(JBLabel("Strong similarity boundary percentage: "), _strongSimilarityBoundaryPercentage)
        .addComponentFillVertically(JPanel(), 0)
        .panel

    val preferredFocusedComponent
        get() = _ignoreWhitespaces

    var ignoreWhitespaces
        get() = _ignoreWhitespaces.isSelected
        set(value) {
            _ignoreWhitespaces.isSelected = value
        }

    var leafNodesSimilarityThreshold
        get() = _leafNodesSimilarityThreshold.value
        set(value) {
            _leafNodesSimilarityThreshold.value = value
        }

    var innerNodesSimilarityThreshold
        get() = _innerNodesSimilarityThreshold.value
        set(value) {
            _innerNodesSimilarityThreshold.value = value
        }

    var similarityBoundaryCoefficient
        get() = _similarityBoundaryCoefficient.value
        set(value) {
            _similarityBoundaryCoefficient.value = value
        }

    var strongSimilarityBoundaryPercentage
        get() = _strongSimilarityBoundaryPercentage.value
        set(value) {
            _strongSimilarityBoundaryPercentage.value = value
        }
}

class TenthSlider(min: Int, max: Int): JBSlider(min, max) {
    init {
        labelTable = Hashtable<Int, JLabel>()
        labelTable.put(min, JLabel("${min.toDouble() / 10}"))
        labelTable.put(max, JLabel("${max.toDouble() / 10}"))
    }
}
