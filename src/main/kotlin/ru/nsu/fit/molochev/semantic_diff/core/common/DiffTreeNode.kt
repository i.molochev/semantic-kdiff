package ru.nsu.fit.molochev.semantic_diff.core.common

import com.intellij.psi.PsiElement
import com.intellij.psi.PsiWhiteSpace
import com.intellij.psi.tree.IElementType
import com.intellij.refactoring.suggested.endOffset
import com.intellij.refactoring.suggested.startOffset
import org.jetbrains.kotlin.psi.psiUtil.allChildren
import ru.nsu.fit.molochev.semantic_diff.lang.LanguageSpecificExtensionsProvider
import kotlin.reflect.KClass

abstract class DiffContextLevel(val ctxHolder: DiffTreeNode, val value: String?) {

    override fun equals(other: Any?): Boolean {
        if (other !is DiffContextLevel || other::class != this::class) return false
        if (ctxHolder.id != null) {
            return ctxHolder.id == other.ctxHolder.id
        } else {
            if (other.ctxHolder.id != null) return false
            return value == other.value
        }
    }

    override fun hashCode(): Int {
        return (this::class.hashCode() * 54 + (ctxHolder.id?.hashCode() ?: 0)) * 43 + (value?.hashCode() ?: 0)
    }
}

data class DiffTreeNode(var value: PsiElement) {

    private val _children: MutableList<DiffTreeNode> = arrayListOf()
    val children: List<DiffTreeNode>
        get() = _children

    var parent: DiffTreeNode? = null
        private set

    val label: IElementType
        get() = value.node.elementType

    val text: String = value.text.toString()

    var id: String? = null

    var contextLevel: DiffContextLevel? = null

    fun isLeaf(): Boolean = children.isEmpty()

    fun isSingle(): Boolean = isLeaf() || children.size == 1 && children[0].isSingle()

    fun height(): Int = (children.map(DiffTreeNode::height).maxOrNull() ?: 0) + 1

    fun sentence(): List<DiffTreeNode> {
        return if (isLeaf()) listOf(this)
        else children.flatMap(DiffTreeNode::sentence)
    }

    fun haveParent(x: DiffTreeNode): Boolean {
        if (parent == null) return false
        if (parent === x) return true
        return parent!!.haveParent(x)
    }

    fun addChild(node: DiffTreeNode, k: Int? = null) {
        if (k == null) {
            _children.add(node)
        }
        else {
            _children.add(k, node)
        }
        node.parent = this
    }

    fun removeChild(node: DiffTreeNode) {
        _children.remove(node)
        node.parent = null
    }

    fun moveTo(node: DiffTreeNode, k: Int) {
        parent?.removeChild(this)
        node.addChild(this, k)
    }

    fun nodes(): List<DiffTreeNode> {
        val result = mutableListOf(this)
        children.forEach {
            result.addAll(it.nodes())
        }
        return result
    }

    fun copy(): DiffTreeNode {
        val newNode = DiffTreeNode(value)
        newNode.id = this.id
        return newNode
    }

    override fun toString(): String {
        return "$label: \"$text\" (${value.startOffset}:${value.endOffset})"
    }
}

fun PsiElement.toDiffTreeNode(provider: LanguageSpecificExtensionsProvider, ignoreWhitespaces: Boolean): DiffTreeNode {
    val node = DiffTreeNode(this.copy())
    provider.childrenIterator(this).forEach { child ->
        if (ignoreWhitespaces && child.isWhitespace() ||
            provider.nodesToIgnore.any { child.isOfType(it) }) {
            return@forEach
        }

        node.addChild(child.toDiffTreeNode(provider, ignoreWhitespaces))
    }
    node.identify(provider)
    return node
}

private fun DiffTreeNode.identify(provider: LanguageSpecificExtensionsProvider) {    // TODO: think about it
    run {
        if (value.isWord(provider)) {
            id = value.text.toString()
            return@run
        }

        if (isSingle()) {
            children.find { it.id != null }?.let {
                id = it.id
            }
            return@run
        }

        val ids = children.filter { it.value.isWord(provider) }
        if (ids.size == 1) {
            id = ids.first().id
        }
    }

    provider.additionalNodeIdentifier(this)
}

fun DiffTreeNode.initContext(provider: LanguageSpecificExtensionsProvider): DiffTreeNode {
    val initCtx = provider.contextLevelInitializer
    this.initCtx()
    children.forEach { it.initCtx() }
    return this
}

fun PsiElement.isOfType(type: KClass<out PsiElement>) = this::class == type

private fun PsiElement.isWord(provider: LanguageSpecificExtensionsProvider) = provider.isWord(this.node.elementType)

private fun PsiElement.isWhitespace() = node is PsiWhiteSpace
