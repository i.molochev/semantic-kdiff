package ru.nsu.fit.molochev.semantic_diff.core.editscript

import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiElement
import com.intellij.refactoring.suggested.endOffset
import com.intellij.refactoring.suggested.startOffset
import ru.nsu.fit.molochev.semantic_diff.core.common.DiffTreeNode


enum class EditOperationType {
    INSERT, MOVE, DELETE, UPDATE
}

sealed class EditOperation {

    abstract fun exec()
    abstract val leftStart: Int
    abstract val leftEnd: Int
    abstract val rightStart: Int
    abstract val rightEnd: Int
    abstract val src: DiffTreeNode
    open val dst: DiffTreeNode? = null
    open val index: Int = -2

    abstract val type: EditOperationType

    fun leftRange() = TextRange(leftStart, leftEnd)

    fun rightRange() = TextRange(rightStart, rightEnd)
}

class Insert(
    private val node: DiffTreeNode,
    private val parent: DiffTreeNode,
    override val index: Int
) : EditOperation() {

    override val type = EditOperationType.INSERT

    override fun exec() {
        parent.addChild(node, index)
    }

    override val leftStart = /*if (index == 0) parent.value.startOffset else parent.children[index - 1].value.endOffset*/ 0
    override val leftEnd = leftStart
    override val rightStart = node.value.startOffset
    override val rightEnd = node.value.endOffset
    override val src = node
    override val dst = parent

    override fun toString(): String {
        return "INSERT ${node.label}(${node.value.startOffset}:${node.value.endOffset})" +
                " as $index-th child of " +
                "${parent.label}(${parent.value.startOffset}:${parent.value.endOffset})"
    }
}

class Move(
    private val node: DiffTreeNode,
    private val newParent: DiffTreeNode,
    override val index: Int,
    newPlaceStart: Int,
    newPlaceEnd: Int
) : EditOperation() {

    override val type = EditOperationType.MOVE

    override fun exec() {
        node.moveTo(newParent, index)
    }

    override val leftStart = node.value.startOffset
    override val leftEnd = node.value.endOffset
    override val rightStart = newPlaceStart
    override val rightEnd = newPlaceEnd
    override val src = node
    override val dst = newParent

    override fun toString(): String {
        return "MOVE ${node.label}(${node.value.startOffset}:${node.value.endOffset})" +
                " as $index-th child of " +
                "${newParent.label}(${newParent.value.startOffset}:${newParent.value.endOffset})"
    }
}

class Delete(
    private val node: DiffTreeNode
) : EditOperation() {

    override val type = EditOperationType.DELETE

    val oldParent = node.parent

    init {
        if (!node.isLeaf()) {
            throw IllegalArgumentException("Not a leaf node")
        }
    }

    override fun exec() {
        node.parent?.removeChild(node)
    }

    override val leftStart = node.value.startOffset
    override val leftEnd = node.value.endOffset
    override val rightStart = /*node.parent?.let { parent ->
    val index = parent.children.indexOf(node)
    if (index == 0) parent.value.startOffset else parent.children[index - 1].value.endOffset
  } ?:*/ 0
    override val rightEnd = rightStart
    override val src = node
    override val index = node.parent?.children?.indexOf(node) ?: -2

    override fun toString(): String {
        return "DELETE ${node.label}(${node.value.startOffset}:${node.value.endOffset})"
    }
}

class Update(
    private val node: DiffTreeNode,
    private val value: PsiElement
) : EditOperation() {

    override val type = EditOperationType.UPDATE

    init {
        if (!node.isLeaf()) {
            throw IllegalArgumentException("Not a leaf node")
        }
    }

    override fun exec() {
        node.value = value
    }

    override val leftStart = node.value.startOffset
    override val leftEnd = node.value.endOffset
    override val rightStart = value.startOffset
    override val rightEnd = value.endOffset
    override val src = node
    override val dst = node
    override val index = node.parent?.children?.indexOf(node) ?: -2

    override fun toString(): String {
        return "UPDATE ${node.label}(${node.value.startOffset}:${node.value.endOffset})" +
                "to value '${node.value.text}'"
    }
}
