package ru.nsu.fit.molochev.semantic_diff.actions

import com.intellij.ide.plugins.newui.HorizontalLayout
import com.intellij.notification.Notification
import com.intellij.notification.NotificationType
import com.intellij.notification.Notifications
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.editor.Document
import com.intellij.openapi.editor.EditorFactory
import com.intellij.openapi.fileEditor.FileDocumentManager
import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.openapi.progress.ProgressManager
import com.intellij.openapi.progress.Task
import com.intellij.openapi.project.DumbAwareAction
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.DialogWrapper
import com.intellij.psi.PsiDocumentManager
import com.intellij.ui.components.Label
import org.jdesktop.swingx.VerticalLayout
import ru.nsu.fit.molochev.semantic_diff.core.DiffCallback
import ru.nsu.fit.molochev.semantic_diff.core.SemanticDiffBuilder
import ru.nsu.fit.molochev.semantic_diff.core.editscript.SemanticSideBySideDiff
import ru.nsu.fit.molochev.semantic_diff.lang.KotlinDiffProvider
import ru.nsu.fit.molochev.semantic_diff.settings.SemanticDiffSettingsComponent
import ru.nsu.fit.molochev.semantic_diff.settings.SemanticDiffSettingsState
import javax.swing.JButton
import javax.swing.JComponent
import javax.swing.JPanel

class CompareFilesSemanticallyAction : DumbAwareAction() {

    override fun update(e: AnActionEvent) {
        val files = e.getData(CommonDataKeys.VIRTUAL_FILE_ARRAY)
        val canShow = files != null && files.size == 2
        e.presentation.apply {
            isEnabled = canShow
            isVisible = canShow
        }
    }

    override fun actionPerformed(e: AnActionEvent) {
        val project = e.project
        if (project == null) {
            Notifications.Bus.notify(
                Notification(
                    "diff.test.notifications",
                    "You cannot run semantic diff without a project!",
                    NotificationType.ERROR
                )
            )
            return
        }

        val files = e.getData(CommonDataKeys.VIRTUAL_FILE_ARRAY)
        if (files == null || files.size != 2) {
            Notifications.Bus.notify(
                Notification(
                    "diff.test.notifications",
                    "You must select two files to perform semantic diff!",
                    NotificationType.ERROR
                )
            )
            return
        }

        val leftFile = files[0]
        val leftDocument = FileDocumentManager.getInstance().getDocument(leftFile)
        if (leftDocument == null) {
            Notifications.Bus.notify(
                Notification(
                    "diff.test.notifications",
                    "Cannot open file ${leftFile.name}!",
                    NotificationType.ERROR
                )
            )
            return
        }

        val rightFile = files[1]
        val rightDocument = FileDocumentManager.getInstance().getDocument(rightFile)
        if (rightDocument == null) {
            Notifications.Bus.notify(
                Notification(
                    "diff.test.notifications",
                    "Cannot open file ${rightFile.name}!",
                    NotificationType.ERROR
                )
            )
            return
        }

        DiffDialog(project, leftDocument, rightDocument).show()
    }

    class DiffDialog(
        private val p: Project,
        private val leftDocument: Document,
        private val rightDocument: Document,
    ) : DialogWrapper(p) {

        init {
            title = "Semantic Diff"
            init()
        }

        override fun createCenterPanel(): JComponent {
            val leftEditor = EditorFactory.getInstance().createViewer(leftDocument)
            val rightEditor = EditorFactory.getInstance().createViewer(rightDocument)

            val manager = PsiDocumentManager.getInstance(p)
            val leftPsi = manager.getPsiFile(leftDocument)
            val rightPsi = manager.getPsiFile(rightDocument)

            if (leftPsi == null || rightPsi == null) {
                return Label("Cannot get PSI for provided files")
            }

            val diff = try {
                ProgressManager.getInstance().run(object : Task.WithResult<SemanticSideBySideDiff, Exception>
                    (p, "Building Semantic Diff", false) {
                    override fun compute(indicator: ProgressIndicator): SemanticSideBySideDiff {
                        indicator.text = "Converting PSI"
                        indicator.fraction = 0.0

                        val builder = SemanticDiffBuilder()

                        val settings = SemanticDiffSettingsState.instance
                        builder.config(settings.toConfig())

                        builder.addProvider(KotlinDiffProvider())

                        val result = builder.buildSemanticSideBySideDiff(leftPsi, rightPsi, settings.ignoreWhitespaces,
                            object : DiffCallback {
                                override fun conversionFinished() {
                                    indicator.text = "Creating matching"
                                    indicator.fraction = 0.15
                                }

                                override fun matchingFinished() {
                                    indicator.text = "Creating editscript"
                                    indicator.fraction = 0.50
                                }

                                override fun editScriptFinished() {
                                    indicator.text = "Converting result"
                                    indicator.fraction = 0.9
                                }
                            })
                        indicator.fraction = 1.0
                        return result
                    }
                })
            } catch (e: IllegalArgumentException) {
                return Label(e.message ?: "Cannot build diff on passed files")
            }

            addHighlighting(diff, leftEditor, rightEditor)

            val panel = JPanel(HorizontalLayout(10))
            panel.add(leftEditor.component)
            panel.add(rightEditor.component)
            return panel
        }
    }
}
