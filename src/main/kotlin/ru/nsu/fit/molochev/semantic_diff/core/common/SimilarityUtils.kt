package ru.nsu.fit.molochev.semantic_diff.core.common

import ru.nsu.fit.molochev.semantic_diff.core.mathcing.Matching
import kotlin.math.max

fun commonScore(x: DiffTreeNode, y: DiffTreeNode, matching: Matching): Double {
    val maxLength = max(x.sentence().size, y.sentence().size)

    val commonCount = matching.pairs
        .count { it.first.isLeaf() && it.second.isLeaf() && it.first.haveParent(x) && it.second.haveParent(y) }
    return commonCount * 1.0 / maxLength
}

fun levenshteinSimilarity(x: CharSequence, y: CharSequence): Double =
    1.0 - levenshteinDistance(x, y) * 1.0 / max(x.length, y.length)

private fun levenshteinDistance(x: CharSequence, y: CharSequence): Int {
    val dp = Array(x.length + 1) { IntArray(y.length + 1) }
    for (i in 0..x.length) {
        for (j in 0..y.length) {
            dp[i][j] = when {
                i == 0 -> j
                j == 0 -> i
                else -> {
                    val cost = if (x[i - 1] == y[j - 1]) 0 else 1
                    minOf(dp[i - 1][j - 1] + cost, dp[i - 1][j] + 1, dp[i][j - 1] + 1)
                }
            }
        }
    }
    return dp[x.length][y.length]
}

fun ngramsSimilarity(x: CharSequence, y: CharSequence, n: Int): Double {
    if (x == y) return 1.0
    val xNgrams = ngrams(x, n)
    val yNgrams = ngrams(y, n)
    val intersection = xNgrams intersect yNgrams
    val union = xNgrams union yNgrams
    return 2.0 * intersection.size / union.size
}

private fun ngrams(x: CharSequence, n: Int): Set<CharSequence> {
    val ngrams = hashSetOf<CharSequence>()
    for (i in 0..x.length - n) {
        ngrams.add(x.subSequence(i, i + n))
    }
    return ngrams
}
