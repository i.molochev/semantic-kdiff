package ru.nsu.fit.molochev.semantic_diff.lang

import com.intellij.lang.Language
import com.intellij.psi.PsiElement
import com.intellij.psi.tree.IElementType
import ru.nsu.fit.molochev.semantic_diff.core.common.DiffTreeNode
import ru.nsu.fit.molochev.semantic_diff.core.editscript.EditScript
import ru.nsu.fit.molochev.semantic_diff.core.mathcing.Matching
import kotlin.reflect.KClass

abstract class LanguageSpecificExtensionsProvider(val language: Language) {

    abstract val contextLevelInitializer: DiffTreeNode.() -> Unit
    abstract val contextNodeComparator: (DiffTreeNode, DiffTreeNode) -> Boolean
    abstract val specificNodeEqualizer: (DiffTreeNode, DiffTreeNode) -> Boolean

    abstract val childrenIterator: (PsiElement) -> Iterator<PsiElement>
    abstract val isWord: (IElementType) -> Boolean
    abstract val nodesToIgnore: List<KClass<out PsiElement>>
    abstract val uniqueNodes: List<KClass<out PsiElement>>
    abstract val additionalNodeIdentifier: (DiffTreeNode) -> Unit
    abstract val specificNodePostprocessor: (List<DiffTreeNode>, List<DiffTreeNode>, Matching) -> Unit
    abstract val editScriptOptimizer: (EditScript) -> EditScript
}
