package ru.nsu.fit.molochev.semantic_diff.core

import com.intellij.lang.Language
import com.intellij.psi.PsiElement
import ru.nsu.fit.molochev.semantic_diff.core.common.initContext
import ru.nsu.fit.molochev.semantic_diff.core.common.toDiffTreeNode
import ru.nsu.fit.molochev.semantic_diff.core.editscript.SemanticSideBySideDiff
import ru.nsu.fit.molochev.semantic_diff.core.editscript.buildScript
import ru.nsu.fit.molochev.semantic_diff.core.editscript.convert
import ru.nsu.fit.molochev.semantic_diff.core.mathcing.MatchingBuilder
import ru.nsu.fit.molochev.semantic_diff.lang.LanguageSpecificExtensionsProvider

class SemanticDiffBuilder {

    private var matchingBuilder = MatchingBuilder(DiffConfig())

    private val languageProviders = mutableMapOf<Language, LanguageSpecificExtensionsProvider>()

    fun config(config: DiffConfig) {
        matchingBuilder = MatchingBuilder(config)
    }

    fun addProvider(provider: LanguageSpecificExtensionsProvider) {
        languageProviders[provider.language] = provider
    }

    fun buildSemanticSideBySideDiff(
        left: PsiElement,
        right: PsiElement,
        ignoreWhitespaces: Boolean = false,
        callback: DiffCallback
    ): SemanticSideBySideDiff {
        if (left.language != right.language) {
            throw IllegalArgumentException()
        }
        val provider = languageProviders[left.language]
            ?: throw IllegalArgumentException("Language not supported: ${left.language}")

        val t1 = left.toDiffTreeNode(provider, ignoreWhitespaces).initContext(provider)
        val t2 = right.toDiffTreeNode(provider, ignoreWhitespaces).initContext(provider)
        callback.conversionFinished()
        val matching = matchingBuilder.match(t1, t2, provider)
        callback.matchingFinished()
        val editScript = buildScript(t1, t2, matching)
        callback.editScriptFinished()
        return convert(editScript, provider)
    }
}

interface DiffCallback {
    fun conversionFinished()
    fun matchingFinished()
    fun editScriptFinished()
}

data class DiffConfig(
    /**
     * Determines threshold for checking similarity of the text of leaf nodes. [0; 1]
     *
     * 0 - any pair of texts are considered equal, 1 - texts needs complete equality to pass the check.
     */
    val leafNodesSimilarityThreshold: Double = 0.6,

    /**
     * Determines percentage of matched descendant node pairs for checking equality of non-leaf nodes. [0.5; 1]
     *
     * 0 - any pair of inner nodes is considered equal, 1 - all nodes' descendants must be matched to consider nodes as equal.
     */
    val innerNodesSimilarityThreshold: Double = 0.2,

    val similarityBoundaryCoefficient: Double = 2.0,

    val enableChangeDistilling: Boolean = false,

    val strongSimilarityBoundaryPercentage: Double = 0.9,

    val ngramsCount: Int = 2,

    /**
     * Enable dynamic similarity calculation for inner nodes (used for ChangeDistiller algo).
     */
    val isDynamicSimilarityEnabled: Boolean = true,

    /**
     * Dynamic similarity threshold for inner nodes. Ignored if [isDynamicSimilarityEnabled] is false.
     */
    val dynamicSimilarityThreshold: Double? = 0.4,

    /**
     * Bound depth for inner nodes to apply dynamic similarity threshold. Ignored if [isDynamicSimilarityEnabled] is false.
     */
    val dynamicSimilarityDepthBound: Int? = 4
) {

    init {
        if (isDynamicSimilarityEnabled && (dynamicSimilarityThreshold == null || dynamicSimilarityDepthBound == null)) {
            throw IllegalArgumentException("Illegal dynamic similarity settings: threshold or depth is null!")
        }
    }

}
